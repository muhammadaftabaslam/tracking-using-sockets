var timers = require("timers"),
    http = require("http"),
 fileio = require('./fileio'),
    ___backgroundTimer,
	util = require('util'),
	redis_store = require('redis'),
redis = redis_store.createClient();
 
process.on('message',function(msg){
	var sortSequence = function(myObj,callback)
	{
		var keys = [],
		k, i, len;
		for (k in myObj)
		{
			if (myObj.hasOwnProperty(k))
			{
				keys.push(k);
			}
		}
		keys.sort();
		len = keys.length;
		var obj = {};
		for (i = 0; i < len; i++)
		{
			k = keys[i];
			obj[k] =  myObj[k];
		}
		callback(obj);
	}
	var getValues = function(json,callback){
try{
    var str = '';
    var count = 0;
    Object.keys(json).forEach(function(col){
        str += json[col]+'`^';
        count++;
        if(count == Object.keys(json).length){
            str = str.substr(0,str.length-1);
            str += '\r\n';
            callback(str);
        }
    });
 }
 catch(er)
 {
  console.log("cron : getheaders : ",er);
 }
}

var ConvertToCSVtostring = function(Array,callback) {
 try{
 console.log("ConvertToCSV",Array.length);
	if(Array.length != 0)
	{
		//var str =  "MInfo"+"`^"+"chatDuration"+"`^"+"count"+"`^"+"WebsiteId"+"`^"+"IP"+"`^"+"sessionid"+"`^"+"VisitorID"+"`^"+"socketid"+"`^"+"Browser"+"`^"+"startTime"+"`^"+"DateTime"+"`^"+"DateTimeOnSite"+"`^"+"LastActivityTime"+"`^"+"BrowserEngine"+"`^"+"OS"+"`^"+"URL"+"`^"+"Referrer"+"`^"+"endTime"+"`^"+"IsMobile"+'\r\n';
		var str = "";
		for(var i=0;i<Array.length;i++)
		{
			
					Array[i].socketid = "";
					Array[i].SearchTracking = "";
					Array[i].BehaviorTracking = "";
						sortSequence(Array[i],function(val){
							getValues(val,function(lastval){
								fileio.InsertInFileforBasicInfo(lastval,function(){
								//console.log("job done");
								})
							});
						});
		
		}
		//console.log("str",str);
		callback(str);
	}
	else{
	console.log("cron.js : length is less than 1");
	}
	 
        
 }
 catch(er)
 {
  console.log("Cron.js : ConvertToCSV : ",er);
 }
}


	var basicInformation = function(){

 try
 {
		//console.log("basicinfo");
        var multi = redis.multi();
		var multi_del = redis.multi();
        redis.HGETALL("visitorIds", function(err,result) {
		//console.log("results with result",result);
            var length = 0;
			if(typeof result === 'object' && result !== null)
			{
            Object.keys(result).forEach(function(vid) {
                length++;
                if (length == Object.keys(result).length) {
                   multiFun();
                } else {
                    multi.HGETALL("Track:" + vid);
					multi_del.del("BT:" + vid);
					multi_del.del("SK:" + vid);				
					//multi_del.del("TM-Current:" + vid);
					multi_del.del("TM-History:" + vid);
                }
				
            })
			}
			else{
			//console.log("some problem");
			}
        });
   function multiFun()
                {
                    multi.exec(function(error, results)
                        { 
                            if(!error)
                            {
								
								ConvertToCSVtostring(results,function(csv){
                                multi_del.exec(function(err,result){
											console.log('multi_del finished.'); 
                                            redis.del("visitorIds",function(err,resultss){
											console.log("visitorIds",err,resultss);                                              
								
                                    });     
                                });  
});								
                            }
							else
							{
								console.log("Cron : Multifun : ",error);
							}   
                        });   
                }
  }
 catch(er)
 {
  console.log("Cron : basicinformation : ",er);
 }
}












       this._longRunningTask = function(data){
	try{
	
		//console.log("Cron Job: Time Over or complete");
		redis.strlen("TM:Allwebsites",function(err,len){
			//console.log(" child process:  length is : ",len);
			
				if(len > 0)
				{
					//console.log("len is < than TM");
					redis.get("TM:Allwebsites",function(err,alldata){
						fileio.InsertInFile(alldata,"TM",function(){
							redis.del("TM:Allwebsites",function(a,b){/*console.log("keys  delete",a,b);*/});				
						});  
					});	
				}
				});		
				redis.strlen("BT:Allwebsites",function(err,len){
				//console.log(" child process:  length is : ",len);
				if(len > 0)
				{				
					//console.log("len is < than BT");
					redis.get("BT:Allwebsites",function(err,alldata){
						fileio.InsertInFile(alldata,"BT",function(){
							redis.del("BT:Allwebsites",function(a,b){/*console.log("keys  delete",a,b);*/});				
						});  
					});	
					}
				});	
				redis.strlen("SK:Allwebsites",function(err,len){
					//console.log(" child process:  length is : ",len);
				if(len > 0)
				{		
					//console.log("len is < than SK");
					redis.get("SK:Allwebsites",function(err,alldata){
						fileio.InsertInFile(alldata,"SK",function(){
							redis.del("SK:Allwebsites",function(a,b){/*console.log("keys  delete",a,b);*/});				
						});  
					});	
					}
				});
				//basicInformation();
			}
		 catch(er)
		 {
		  console.log("Cron : _longRunningTask : ",er);
		 }
    }
	this._checkingLength = function(data){
	try{
	
	//console.log("TM length checking");
		redis.strlen("TM:Allwebsites",function(err,len){
			//console.log(" child process:  length is : ",len);
				if(len > 2500000)
				{
					
					redis.get("TM:Allwebsites",function(err,alldata){
						fileio.InsertInFile(alldata,"TM",function(){
							redis.del("TM:Allwebsites",function(a,b){/*console.log("keys  delete",a,b);*/});				
						});  
					});							
				}
				else
				{
					//console.log(" child processs length is less than the limit TM");
				}
			});
	
	
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//console.log(" BT length checking");
		redis.strlen("BT:Allwebsites",function(err,len){
		if(!err)
		{
			//console.log(" child process:  length is : ",len);
			
				if(len > 2500000)
				{
					
					redis.get("BT:Allwebsites",function(error,alldata){
					if(!error)
					{
						fileio.InsertInFile(alldata,"BT",function(){
							redis.del("BT:Allwebsites",function(a,b){/*console.log("keys  delete",a,b);*/});				
						});  
						
					}
					else
					{
						console.log("child processs check len BT get error",error);
					}
					});							
				}
				else
				{
					//console.log(" child processs length is less than the limit BT");
				}
			}
			else
			{
				console.log("child processs check len BT STRLEN error",err);
			}
		});
	
		
		
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////		
	//console.log("SK length checking");
		redis.strlen("SK:Allwebsites",function(err,len){
			//console.log(" child process:  length is : ",len);
				if(len > 2500000)
				{
					
					redis.get("SK:Allwebsites",function(err,alldata){
						fileio.InsertInFile(alldata,"SK",function(){
							redis.del("SK:Allwebsites",function(a,b){/*console.log("keys  delete",a,b);*/});				
						});  
					});							
				}
				else
				{
					//console.log(" child processs length is less than the limit SK");
				}
			});
			//basicInformation();
			//updateChatDuration();
			redis.hlen("visitorIds",function(err,len){
			console.log(" visitorIDs:  length is : ",err,len);
				if(len > 5000)
				{
					//basicInformation();
										
				}
				else
				{
					//console.log(" child processs length is less than the limit SK");
				}
			});
		}
		 catch(er)
		 {
		  console.log("Cron : _longRunningTask : ",er);
		 }
    }
 
    this._startTimer = function(){
        var count = 0;
		 //console.log("Start timer");
        ___backgroundTimer = timers.setInterval(function(){
 
            try{
                var date = new Date();
                //console.log("Cron.js: datetime tick: " + date.toUTCString());
                //this._longRunningTask(msg.content);
            }
            catch(err){
                count++;
                if(count == 3){
                    //console.log("Cron.js: shutdown timer...too many errors. " + err.message);
                    clearInterval(___backgroundTimer);
                    process.disconnect();
                }
                else{
                    console.log("Cron.js error: " + err.message + "\n" + err.stack);
                }
            }
        }.bind(this),msg.interval);
    }
	this._startTimerforlen = function(){
        var count = 0;
		 //console.log("Start timer");
        ___backgroundTimer = timers.setInterval(function(){
 
            try{
                var date = new Date();
                //console.log("Cron.js: datetime tick: " + date.toUTCString());
                this._checkingLength(msg.content);
            }
            catch(err){
                count++;
                if(count == 3){
                    //console.log("Cron.js: shutdown timer...too many errors. " + err.message);
                    clearInterval(___backgroundTimer);
                    process.disconnect();
                }
                else{
                    console.log("Cron.js error: " + err.message + "\n" + err.stack);
                }
            }
        }.bind(this),msg.intervalforlen);
    }
 
	
    this._init = function(){
	 //console.log("init function");
        if(msg.content != null || msg.content != "" && msg.start == true){
            this._startTimer();
			this._startTimerforlen();
        }
        else{
            //console.log("Cron.js: content empty. Unable to start timer.");
        }
    }.bind(this)()
 
});

 
process.on('uncaughtException',function(err){

 console.log("uncaught exception");
    console.log("Cron.js: " + err.message + "\n" + err.stack + "\n Stopping background timer");
    //clearInterval(___backgroundTimer);
});