var stats_lib = require('./stats_lib'),	
	visitor_stats = require('./visitor');
	
exports.TimeTracking = function(data)
{
try{
	//console.log('i am TimeTracking, received the data '.green,data);
	//console.log('Tm Stats.js '.green);
	stats_lib.hashExists(data.visitorid,function(hashExist)// Hash exist function
	{
		var multi = redis.multi();
		var datetimeUTC = new Date().toUTCString();
		redis.hset("visitorIds",data.visitorid,1);
		if(hashExist)
		{
			redis.hget("Track:"+data.visitorid,"LastActivityTime",function(err,value)
				{
					var old = new Date(value);
					var newtime = new Date();

					//console.log("Hash Exist");
					if(!err)
					{
						
						if(newtime.getTime() - old.getTime() > 900000)
						{
							//console.log("change 3 fields".red);
							SaveRedis(data,data.visitorid,function(){});
						}
						else
						{
							//console.log("Hash Exist -Not error: ".red,value);
							multi.hset("Track:"+data.visitorid,"LastActivityTime",datetimeUTC);
							multi.zadd("Online:"+parseInt(newtime.getTime()/60000),data.websiteid,data.visitorid);
							multi.expire("Online:"+parseInt(newtime.getTime()/60000),120);
							multi.exec(function(err, results)
							{
								if(err)
								{
									console.log("Multi Tm-stats if".red,err);
								}
							}); 
						}
					}
					
				});
			

			visitor_stats.SaveVisitorTMStats(data,function(reply){});
		
		}
		else
		{
			SaveRedis(data,data.visitorid,function(){});
			visitor_stats.SaveVisitorTMStats(data,function(){});//TM for saving Data against visitor 
		}
	});	
	
	}
	catch(er)
	{
		console.log("tm_stats : TimeTracking : ",er);
	}
}
	
	
function SaveRedis(data,visitorid,callback)
{
		var multi = redis.multi();
		var newtime = new Date();
		var datetimeUTC = new Date().toUTCString();
							//console.log("Save time is more than 5 mints need to make new session");
							multi.hset("Track:"+visitorid,"DateTimeOnSite",datetimeUTC);
							multi.hincrby("Track:"+visitorid,"sessionid",1);
							multi.expire("Online:"+parseInt(newtime.getTime()/60000),120);
							multi.zadd("Online:"+parseInt(newtime.getTime()/60000),data.websiteid,data.visitorid);
							multi.hset("Track:"+visitorid,"LastActivityTime",datetimeUTC);
							multi.exec(function(err, results)
							{
								if(err)
								{
									console.log("Multi Tm-stats if".red,err);
								}
								else
								{
									callback();
								}
							}); 
}
