var redisio = require('./redis');
var fileio = require('./fileio');
// Sending current data or each post to socket if someone listen to that visitor
exports.CurrentData = function(visitorid,data,callback) // function if socket exist for a visitor and is still alive the push data
{
	try{
	redisio.GetHashFields("Track:"+visitorid,"socketid",function(socketid)
	{
		//console.log("socketid",socketid);
		if(socketid !== "")
		{
			if(io.sockets.sockets[socketid] == undefined)
			{
				//console.log(" Current Data cannot emitted".red);
				// redis.hdel("Ref:"+visitorid, "socketid",function(err,rem){
					// callback();
				// });
			}
			else
			{
					//console.log("CurrentData Emit data on socket".red,data);
					fileio.appendFile(JSON.stringify(data),visitorid,"Serve client",function(){});
					io.sockets.sockets[socketid].emit('VisitorData',data);
					callback();
			}
		}
		else
		{
			console.log("CurrentData No socket for "+visitorid+" visitor SocketServer  ".red);
			callback();
		}
	});
	}
	catch(er)
	{
		console.log("sockeserver : CurrentData : ",er);
	}
}