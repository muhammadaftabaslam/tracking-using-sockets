
exports.InsertInString = function(data1,callback){
try{
	//console.log("CSV file");
	//console.log("b4".red,data1);
	sortAndSequence(data1,function(data){
	//console.log("after".red,data);
		redis.exists(data.postType+":Allwebsites",function(err,status){
		//console.log('values',err,status);
		if(status)
		{
			//console.log('key exist');
			getValues(data,function(str){
			redis.append(data.postType+":Allwebsites",str,function(err,len){
			//console.log("CSV file : length is".red,len);
				if(len > 2500000) 
				{
					//console.log(" csv file : length is greater than the limit".red,len);
						callback();
				}
				else
				{
					//console.log(" csv file :   length is less than 2000".green);
					callback();
				}

			
				});
			
			});
		
		}
			else{
				//console.log('key does not exist');
				getHeaders(data,function(str){
				
					getValues(data,function(values){
					
						redis.setnx(data.postType+":Allwebsites",str+values,function(err,len){
						callback();
						});
					
					});
				});
			
			}
		});
		});
		}
		 catch(er)
		 {
		  console.log("CSV code : InsertInString : ",er);
		 }


}

var getHeaders = function(json,callback){
//console.log("get headers");
try{
    var str = '';
    var count = 0;
    Object.keys(json).forEach(function(col){
        str += col+'`^';
        count++;
        if(count == Object.keys(json).length){
            str = str.substr(0,str.length-1);
            str += '\r\n';
            callback(str);
        }
    });
 }
 catch(er)
 {
  console.log("cron : getheaders : ",er);
 }
}


var getValues = function(json,callback){
try{
    var str = '';
    var count = 0;
    Object.keys(json).forEach(function(col){
        str += json[col]+'`^';
        count++;
        if(count == Object.keys(json).length){
            str = str.substr(0,str.length-1);
            str += '\r\n';
            callback(str);
        }
    });
 }
 catch(er)
 {
  console.log("cron : getheaders : ",er);
 }
}
var sortAndSequence = function(myObj,callback)
	{
		var keys = [],
		k, i, len;
		for (k in myObj)
		{
			if (myObj.hasOwnProperty(k))
			{
				keys.push(k);
			}
		}
		keys.sort();
		len = keys.length;
		var obj = {};
		for (i = 0; i < len; i++)
		{
			k = keys[i];
			obj[k] =  myObj[k];
		}
		callback(obj);
	}