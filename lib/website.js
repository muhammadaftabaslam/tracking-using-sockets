var stats_lib = require('./stats_lib');
/*
exports.RecordWebsiteWiseStats = function(data){
	//console.log('I am recording website stats');
	
	
	switch(data.postType){
            case 'BT':WebsiteBehavoirTracking(data); break;	
            case 'SK':WebsiteSearchKeywordTracking(data); break;
			case 'TM':WebsiteTimeTracking(data); break;
            default:
	}
}

function WebsiteBehavoirTracking(data)
{
	var RedisKey = "";
	for(var key in data){
		if(Array.isArray(data[key])){
		RedisKey = RedisKey+":"+key;
		}
	}
	if(data.flag == 1){
		stats_lib.IncrWebsiteStats(data.websiteid+":filters",RedisKey,1,function(){});
	}
	else if(data.flag == 2){
	
		stats_lib.IncrWebsiteStats(data.websiteid+RedisKey,data.actionurl,1,function(){});
	}
}

function WebsiteSearchKeywordTracking(data)
{
	if(data.flag==1){
		console.log("website stats: saving SK data",data);
		stats_lib.IncrWebsiteStats(data.websiteid+":keywords",data.searchedtext,1,function(){});
	}else if(data.flag==2){
		stats_lib.IncrWebsiteStats(data.websiteid+":"+data.searchedtext,data.actionurl,1,function(){});
	}
}

function WebsiteTimeTracking(data)
{	
	//condition to get only the first TM post on a page
	if(data.time<2){ 
		stats_lib.IncrWebsiteStats(data.websiteid+":pageVisits",data.actionurl,1,function(){});
		//if the url is that of a product
		if(typeof data.productURL != "undefined" && data.productURL){
			 stats_lib.IncrWebsiteStats(data.websiteid+":productsViewed",data.actionurl,1,function(){});
		}
	}
	//condition to check when a page is left
	if(!data.active){
		stats_lib.IncrWebsiteStats(data.websiteid+":timeSpent",data.actionurl,data.time,function(){});
	}
	//condition to check if this was the first post of a session or not
	if(typeof data.firstVisit != "undefined" && data.firstVisit){
		stats_lib.IncrWebsiteStats("allWebsitesVisits",data.websiteid,1,function(){});
	}
}