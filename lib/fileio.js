fs = require('fs');

function getNowDate(){ 
	try
	{
		//console.log("FileIO : func getNowDate Start");
		var date = new Date;
		var month = date.getUTCMonth() + 1;
		var day = date.getUTCDate();
		var year = date.getUTCFullYear();
		var nowDate = day+"_"+month+"_"+year+"_"+date.getTime();
		console.log("date at this time".red,nowDate);
		//console.log("FileIO : func getNowDate end");
		return nowDate;
	}
	catch(err)
	{
		console.log("FIleIO : GetNowDate : ",err);
	}
}
exports.InsertInFile = function (data,fileName,callback){
	//console.log("FileIO : func InsertInFile Start");
	//console.log("writing file",fileName,data);
	try{
		fs.writeFile("./CSV/"+getNowDate()+"_"+fileName+".csv",data, function(err) {
			if(err) {
				console.log("Error FileIO FILE NOT SAVED : ",err);
			} else {
				//console.log("FileIO The file was saved!");
				callback();
			}
		});
	}
	catch(er)
	{
		console.log("FIleIO : InsertInFile : ",er);
	}
}
exports.appendFile = function(data,visitorid,type,callback){
	try
	{
		var date = new Date;
		fs.appendFileSync("./CSV/socket.log",type+"  :  "+date+" : "+visitorid+"     :    "+data+'\r\n');
		callback();
	}
	catch(er)
	{
		console.log("FIleIO : appendFile : ",er);
	}

}