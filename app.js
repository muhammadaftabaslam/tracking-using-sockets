/**
 * Behavoir Tracking
 * Liveadmins
 * copyright 2014.
 * Author: M. Aftab Aslam
 * Technologies Used
 * Node Server
 * Socket Server
 * Redis Server 
 */

var express = require('express');
var routes = require('./routes/stats');
var http = require('https');
var path = require('path');
var fs = require('fs');
colors = require('colors');
var redis_store = require('redis');
var async = require('async');
var track = require('./lib/tracking');
var lib = require('./lib/stats_lib');
var fileio = require('./lib/fileio');
var socketio = require('./lib/socketServer');
redis = redis_store.createClient();

var numCPUs = require('os').cpus().length;
// console.log("numCPUs".red,numCPUs);
// var port = parseInt(process.argv[2]);
// console.log("port".red,port);
// console.log("process.argv[]".red,process.argv);
// Allow cross domain function
var allowCrossDomain = function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');
    if ('OPTIONS' == req.method) {
      res.send(200);
    }
    else {
      next();
    }
};
// Redis on error function
redis.on('error', function(err){
	console.log(("REDIS ERROR: " + redis.host + " : " + redis.port + " - " + err).red );
});
var app = express();
app.set('port', process.env.PORT || 8099);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.use(allowCrossDomain);
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}
app.use(express.cookieParser());
app.use(express.session({cookie: { path: '/', httpOnly: true, maxAge: null}, secret:'eeuqram'}));
app.use(express.cookieSession({
  key: 'app.sess',
  secret: 'SUPERsekret'
}));
app.post('/ajaxpost',function(req,res){
res.end("ok",function(){});
});
app.get('/ajaxpost',function(req,res){
res.end("ok",function(){});
});
app.get('/',function(req,res){
var newtime = new Date();
	redis.zunionstore("Online",2,"Online:"+parseInt(newtime.getTime()/60000),"Online:"+parseInt((newtime.getTime()/60000)-1),"AGGREGATE","MIN",function(err,val){
						console.log("error",err,val);
					var data = {};
					console.log("time in mints : ".red,parseInt(newtime.getTime()/60000));
					redis.zrange("Online",0,-1,"withscores",function(err,val1){
					for(i=0;i<val1.length;i++)
					{
						i++;
						if(data[val1[i]] == "" || data[val1[i]] == undefined)
						{
							console.log("undefined",data[val1[i]]);
							data[val1[i]] = 1;
						}
						else
						{
							console.log("not undefined",data[val1[i]]);
							data[val1[i]] = data[val1[i]] + 1;
						}
					}
					console.log("after Union",err,data);
					res.send("Node.js Tracking Service : "+JSON.stringify(data));
					redis.del("Online");
					});
					
					});



	
});
var opts = {
  pfx: fs.readFileSync('dealereprocessCertificate1.pfx'),
  passphrase: 'Strong12',
};
const crypto = require('crypto');
var credentials = crypto.createCredentials(opts);
// Node server listen on port 9000
server = http.createServer(opts,function (request, response) {

    console.log(request);

    var status = 404;
    response.writeHead(200, {'Content-Type': 'text/html'});
    response.end('ok');

});
//console.log("read pfx",opts);
server.listen(app.get('port'), function(){
  console.log(('Express server listening on port ' + app.get('port')).green);   

});
//lib.runningCron();
require('https').globalAgent.options.rejectUnauthorized = false; 
io = require('socket.io').listen(server);

io.set('log level', 1);
//io.set('transports', ['websocket','xhr-polling','jsonp-polling']);
   // io.set('reconnect', false);
   // io.set('reconnection limit', 20000);
    //io.set('max reconnection attempts', Infinity);
   // io.set('close timeout', 240);
   // io.set('heartbeat timeout', 240);
   // io.set('heartbeat interval', 120);
   //io.set('transports', ['websocket','xhr-polling','jsonp-polling']);
	//io.set('match origin protocol', true);
//io.set('heartbeats', false);
// io.set('log level', 3);
// io.enable('browser client minification');
// io.enable('browser client etag');
// io.set('transports', [
    // 'websocket',
    // 'htmlfile',
    // 'xhr-polling',
    // 'jsonp-polling',
// ]);

// io.set('log level', 2);
// io.enable('browser client minification');
// io.enable('browser client etag');
// io.set('transports', [
    // 'websocket',
    // 'flashsocket',
    // 'htmlfile',
    // 'xhr-polling',
    // 'jsonp-polling',
// ]);

io.sockets.on('connection', function (socket) { // Function for socket connection with .Net WG chat software
var hs = socket.handshake;
	//console.log("Socket Connection Open.".red,socket.namespace.manager.handshaken[socket.id].address.address,socket.namespace.manager.handshaken[socket.id].headers['user-agent'],hs.headers['user-agent']);
    //console.log("Socket Connection Open");
	socket.on('VisitorID', function (visitorid) {
	console.log("visitor ID on socket request",visitorid);
		lib.hashExists(visitorid,function(hashExist){
			//console.log("hashExist",hashExist);
			if(hashExist)
			{
				redisio.SetHashSingleFields("Track:"+visitorid,"socketid",socket.id,function(){
					//console.log("socket id set : ",socket.id );
					
					redisio.DataPushToOperator(visitorid,function(visitorfiledata) {
						//console.log("Request for the data from Operator : ",visitorid );
						socket.emit('VisitorData',visitorfiledata);
						fileio.appendFile(JSON.stringify(visitorfiledata),visitorid,"On Request",function(){});
					});
				});
			}
			else
			{
				console.log("Hash not found for visitorid " + visitorid);
			}
		});
	});
	socket.on('EndChat', function (visitorid) {
		console.log("socket.on EndChat event received...");
			redis.hdel("Track:"+visitorid, "socketid",function(err,rem){
						console.log("end chat call recieved".red);
					});
		});
	socket.on('ajaxpost', function (data) {
	//console.log("ajax".red,socket.namespace.manager.handshaken[socket.id].headers['user-agent'],hs.headers['user-agent']);
		routes.switchPostType(socket,data);
		});
	socket.on('disconnect', function () {
		console.log("socket.on disconnect event received...");
	});
});